package com.steeplesoft.ktorboot.backend.common.model

data class Dummy(
        var id: Int = -1,
        var name: String
)