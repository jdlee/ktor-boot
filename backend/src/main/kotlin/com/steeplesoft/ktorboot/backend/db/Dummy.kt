package com.steeplesoft.ktorboot.backend.db

import org.jetbrains.squash.definition.TableDefinition
import org.jetbrains.squash.definition.autoIncrement
import org.jetbrains.squash.definition.integer
import org.jetbrains.squash.definition.primaryKey
import org.jetbrains.squash.definition.varchar

object DummyTable : TableDefinition() {
    val id = integer("id").autoIncrement().primaryKey()
    val name = varchar("name", 255)
}