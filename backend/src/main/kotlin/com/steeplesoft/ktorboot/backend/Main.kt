package com.steeplesoft.ktorboot.backend

import com.google.gson.GsonBuilder
import com.steeplesoft.ktorboot.backend.db.Database
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.features.AutoHeadResponse
import io.ktor.features.CallLogging
import io.ktor.features.Compression
import io.ktor.features.ConditionalHeaders
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.features.PartialContentSupport
import io.ktor.gson.GsonConverter
import io.ktor.http.ContentType
import io.ktor.routing.Routing
import io.ktor.server.engine.commandLineEnvironment
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty

fun Application.module() {
    val database = Database(this)

    install(DefaultHeaders)
    install(ConditionalHeaders)
    install(Compression)
    install(PartialContentSupport)
    install(AutoHeadResponse)
    install(CallLogging)
    install(ContentNegotiation) {
        register(ContentType.Application.Json,
                GsonConverter(GsonBuilder().setPrettyPrinting().serializeNulls().create()))
    }

    install(Routing) {
        api(database)
    }
}

fun main(args: Array<String>) {
    val server = embeddedServer(Netty, commandLineEnvironment(args))
    server.start(wait = true)
}