package com.steeplesoft.ktorboot.backend

import com.steeplesoft.ktorboot.backend.db.Database
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.routing.get

fun Routing.api(database : Database) {
    endpoints(database)
}

fun Routing.endpoints(database: Database) {
    get("/") {
        call.respondText("Dummy API", ContentType.Text.Html)
    }
}